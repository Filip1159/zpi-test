FROM node:18.16.0 as build
WORKDIR /ethereum-organisation-manager-front/
COPY public/ /ethereum-organisation-manager-front/public
COPY src/ /ethereum-organisation-manager-front/src
COPY package.json /ethereum-organisation-manager-front/
RUN npm install
RUN npm run build
EXPOSE 3000